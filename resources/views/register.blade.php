<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        
    </head>
    <body>
   <h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>

<form action="/welcome" method="POST">
	@csrf
<label>First name:</label><br><br>
<input type="text" name="firstname"><br><br>

<label>Last name:</label><br><br>
<input type="text" name="lastname"><br><br>

<label for="gender">Gender:</label><br><br>
<input id="male" type="radio" name="gender"><label for="male">Male</label>	 <br>
<input id="female" type="radio" name="gender"><label for="female">Female</label> <br>
<input id="other" type="radio" name="gender"><label for="other">Other</label> <br><br>

<label>Nationality:</label><br><br>
<select name="nationality">
	<option value="indonesia">Indonesia</option>
	<option value="malaysia">Malaysia</option>
	<option value="singapura">Singapura</option>
	<option value="thailand">Thailand</option>
	<option value="vietnam">Vietnam</option>
</select><br><br>

<label>Language Spoken:</label><br><br>
<input type="checkbox">Bahasa Indonesia <br>
<input type="checkbox">English <br>
<input type="checkbox">Other <br><br>

<label>Bio:</label><br><br>
<textarea name="bio" cols="30" rows="10"></textarea><br>

<input type="submit" value="Sign Up">

</form>
    </body>
</html>
